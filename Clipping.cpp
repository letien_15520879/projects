#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if (c1 != 0 && c2 != 0 && c1&c2 != 0)
		return 2;
	return 3;
}

void SwapPoint(Vector2D& P1, Vector2D& P2, CODE &c1, CODE &c2)
{
		Vector2D P;
		P = P1;
		P1 = P2;
		P2 = P;
		CODE c;
		c = c1;
		c1 = c2;
		c2 = c;		
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{	
	int result;
	CODE c1, c2;
	c1 = Encode(r, P1);
	c2 = Encode(r, P2);
	int TH = CheckCase(c1, c2);
	while (TH == 3)
	{
		ClippingCohenSutherland(r, P1, P2);
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		TH = CheckCase(c1, c2);
	}
	if (TH == 1)
	{
		Q1 = P1;
		Q2 = P2;
		result = true;
	}
	else
	{
		result = false;
	}
	return result;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	CODE c1, c2;
	c1 = Encode(r, P1);
	c2 = Encode(r, P2);

	float m = float(P2.y - P1.y) / (P2.x - P1.x);

	if (c1 == 0)
	{
		SwapPoint(P1, P2, c1, c2);
	}
	
	if (c1&LEFT != 0)
	{
		P1.y += (r.Left - P1.x )*m;
		P1.x = r.Left;
		return;
	}
	if (c1&RIGHT != 0)
	{
		P1.y += (r.Right - P1.x)*m;
		P1.x = r.Right;
		return;
	}
	if (c1&TOP != 0)
	{
		P1.x += (r.Top - P1.y)*m;
		P1.y = r.Top;
		return;
	}
	if (c1&BOTTOM != 0)
	{
		P1.x += (r.Bottom - P1.y)/m;
		P1.y = r.Bottom;
		return;
	}
		
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	float r;
	if (p < 0)
	{
		r = float(q) / p;
		if (r > t2)
		{
			return false;
		}
		else
		{
			if (r > t1)
			{
				t1 = r;
			}
		}
	}
	else
	{
		if (p > 0)
		{
			r = float(q) / p;
			if (r < t1)
			{
				return FALSE;
			}
			else
			{
				if (r < t2)
				{
					t2 = r;
				}
			}
		}
		else
		{
			if (q < 0)
			{
				return FALSE;
			}
		}
	}
	return TRUE;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float t1, t2;
	int Dx, Dy, x1, y1, x2, y2, xmin, ymin, xmax, ymax;
	t1 = 0;
	t2 = 1;
	x1 = P1.x;
	y1 = P1.y;
	x2 = P2.x;
	y2 = P2.y;
	Dx = x2 - x1;
	Dy = y2 - y1;
	xmin = r.Left;
	ymin = r.Top;
	xmax = r.Right;
	ymax = r.Bottom;
	if (SolveNonLinearEquation(-Dx, x1 - xmin, t1, t2))
	{
		if (SolveNonLinearEquation(Dx, xmax - x1, t1, t2))
		{
			if (SolveNonLinearEquation(-Dy, y1 - ymin, t1, t2))
			{
				if (SolveNonLinearEquation(Dy, ymax - y1, t1, t2))
				{
					Q1.x = x1 + t1*Dx;
					Q1.y = y1 + t1*Dy;
					Q2.x = x1 + t2*Dx;
					Q2.y = y1 + t2*Dy;
					return true;
				}
			}
		}
	}
	return false;
}
