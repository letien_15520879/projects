#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;

int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);

	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 3;
	rect1->y = p1.y - 3;
	rect1->w = 6;
	rect1->h = 6;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 3;
	rect2->y = p2.y - 3;
	rect2->w = 6;
	rect2->h = 6;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 3;
	rect3->y = p3.y - 3;
	rect3->w = 6;
	rect3->h = 6;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 3;
	rect4->y = p4.y - 3;
	rect4->w = 6;
	rect4->h = 6;

	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };

	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;

	while (running)
	{
		/*set background */
		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);

		SDL_SetRenderDrawColor(ren, colorCurve.a, colorCurve.b, colorCurve.g, colorCurve.r);
		DrawCurve2(ren,p1,p2,p3);
		DrawCurve3(ren, p1, p2, p3, p4);
		
		//*grey color rect1,2,3,4 to control Point P1,P2,P3,P4*/
		SDL_SetRenderDrawColor(ren, colorRect.a, colorRect.b, colorRect.g, colorRect.r);
		SDL_RenderDrawRect(ren, rect1);
		SDL_RenderDrawRect(ren, rect2);
		SDL_RenderDrawRect(ren, rect3);
		SDL_RenderDrawRect(ren, rect4);

		//Red Line between control Point P1 - P2, P2 - P3, P3 - P4 */
		SDL_SetRenderDrawColor(ren, 36, 113, 163, 255);
		SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
		SDL_RenderDrawLine(ren, p2.x, p2.y, p3.x, p3.y);
		SDL_RenderDrawLine(ren, p3.x, p3.y, p4.x, p4.y);

		int xnew, ynew;

		if (SDL_PollEvent(&event))
		{
			//HANDLE MOUSE EVENTS!!!
		
			if (event.button.button == SDL_PRESSED)
			{
				/*get x and y postions from mouse*/
				SDL_GetMouseState(&xnew, &ynew);

				if (abs(p1.x - xnew) < 6 && abs(p1.y - ynew) < 6)
				{
					p1.x = xnew;
					p1.y = ynew;
					rect1->x = p1.x - 3;
					rect1->y = p1.y - 3;
					rect1->w = 6;
					rect1->h = 6;
				}
				if (abs(p2.x - xnew) < 6 && abs(p2.y - ynew) < 6)
				{
					p2.x = xnew;
					p2.y = ynew;
					rect2->x = p2.x - 3;
					rect2->y = p2.y - 3;
					rect2->w = 6;
					rect2->h = 6;
				}
				if(abs(p3.x - xnew) < 6 && abs(p3.y - ynew) < 6)
				{
					p3.x = xnew;
					p3.y = ynew;
					rect3->x = p3.x - 3;
					rect3->y = p3.y - 3;
					rect3->w = 6;
					rect3->h = 6;
				}
				if (abs(p4.x - xnew) < 6 && abs(p4.y - ynew) < 6)
				{
					p4.x = xnew;
					p4.y = ynew;
					rect4->x = p4.x - 3;
					rect4->y = p4.y - 3;
					rect4->w = 6;
					rect4->h = 6;
				}
			}
			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}
		SDL_RenderPresent(ren);
	}
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
