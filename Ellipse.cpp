#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x, y;
	int a2 = a*a;
	int b2 = b*b;
	int a4 = a*a*a*a;

	//vung 1
	x = 0;
	y = b;
	int p = 2 * b2 - 2 * a2*b - a2;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) <= a4)
	{	
		if (p < 0)
		{
			p += 2 * b2*(2 * x + 3);
		}
		else {
			p += 2 * b2*(2 * x + 3) - 4 * a2*y;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}

	//vung 2
	y = 0;
	x = a;
	p = -2 * a*b2 + b2 + 2 * a2;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) >= a4)
	{	
		if (p<0)
		{
			p += 4 * a2*y + 6 * a;
		}
		else
		{
			p += 4 * (a2*y - b2*x + b2) + 6 * a2;
			x = x - 1;
		}
		y = y + 1;
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x = 0;
	int y = b;
	int a2 = a*a;
	int b2 = b*b;
	int a4 = a*a*a*a;

	Draw4Points(xc, yc, x, y, ren);
	int p = int((b2 - (a2*b) + (0.25*a2)) + 0.5);
	while (x*x*(a2 + b2) <= a4)
	{		
		if (p < 0)
		{
			p += b2*(2 * x + 3);
		}
		else
		{	
			p += b2*(2 * x + 3) + a2*(2 - 2 * y);
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	p = (int)((b2*(x + 0.5)*(x + 0.5) + a2*(y - 1)*(y - 1) - a2*b2)+0.5);

	while (y > 0)
	{	
		if (p >= 0)
		{
			p += a2*(3 - 2 * y); 
		}
		else
		{		
			p += b2*(2 * x + 2) + a2*(3 - 2 * y);
			x++;
		}
		y--;
		Draw4Points(xc, yc, x, y, ren);
	}
}