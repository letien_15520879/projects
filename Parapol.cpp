#include "Parabol.h"
#include <iostream>
using namespace std;

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}

void BresenhamDrawParabolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0;
	int y = 0;
	int p = 1 - A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p < 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	while (x <= 200)
	{
		if (p < 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x += 1;
		}
		y += 1;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParabolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0;
	int y = 0;
	int p = 1 - A;
	Draw2Points(xc, yc, x, y, ren);

	while (x <= A)
	{
		if (p < 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x - 2 * A + 3;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}

	while (x <= 200)
	{
		if (p < 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}
